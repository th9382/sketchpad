$(document).ready(function() {

  function build_grid() {
    var count = 16 * 16;
    i = 1;
    $wrapper = $('#wrapper');
    while(i <= count) {
      $wrapper.append("<div class='item'></div>");
      i++;
    }
  };

  build_grid();

  $('div').on('mouseenter', '.item', function() {
    $(this).addClass("mark");
  });

  $('#clear').on('click', function() {
    var marked = $('div').find('.mark').size();
    $('#wrapper').after("<div class='container'>Previously drew on "+marked+" squares.</div>");
    $('div').removeClass('mark');
  });
});
